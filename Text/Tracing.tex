
\chapter{Tracing implementation}
\label{tracing}
\label{KernelTracers}
The policy enforcement part of a sandbox can equally be utilised for tracing purposes. Instead of deciding if a certain call should be allowed to proceed, the call can be logged in some way. 

I need to trace programs on a process-based granularity. Any layer lower than the kernel layer is not usable for this application. 

If I were to sandbox purely at the hypervisor layer, the entire OS startup process would have to be logged as well. The tool would be quite cumbersome to use and require getting information back from the kernel. It would require knowing how the kernel keeps track of processes. But that goes against the principle that a sandbox should consider the code inside it as insecure.

Since this use case is not really suitable for any sort of language based isolation, all that is left is using the kernel layer.

This does not mean that the implementation cannot make use of the hypervisor for sandboxing. Cooperation between the kernel and a hypervisor is possible, as has been demonstrated by\cite{Leon2021}.

The kernel provides many facilities for potential sandboxing of user-space programs. I will explore them here with evaluation of suitability for keeping track of dependencies.
	


\section{User-space based approaches}	
	
	The Linux syscall API provides many ways in which one program may affect another. Some of them could be used to track dependencies.
	
	An issue permeating all of the user-space solutions that would have to be resolved with this approach is keeping a track of relevant PIDs. Linux does not provide any way to obtain the PID of the creator of a given process. So an API which would only attempt to operate on PIDs is insufficient on its own.
\subsection{Inode watchers}
	In a world where the only dependencies which I will try to reproduce are files on the system, just seeing what occurs on the filesystem should be enough.
	
	The Linux kernel has a dedicated inotify\cite{MAN_INOTIFY} API for this purpose. The reason why it cannot be used is that the API cannot distinguish which process had made an interaction. To quote the documentation \directCite{The inotify API provides no information about the user or process that triggered the inotify event.}{MAN_INOTIFY}
	
	Moreover, the watcher gets attached to a specific inode and does not recurse further in the directory tree. A watcher would have to be created for each directory on the entire filesystem.
	And any newly mounted filesystems would be missed.

\subsection{Filesystem watcher}
	Another approach is the newer fanotify\cite{fanotify7} API for watching filesystem events.
	
	The events provided by this API contain the PID of the invoker, so processes interactions could now be tracked. This approach would involve marking all relevant mounts, filesystems, and notifications as tracked. By then logging each notification, a complete list of all filesystem dependencies should be created.
	
	It is to be noted that this API also provides a method for denying access to a given operation. This could be a method in which our sandboxed program could communicate with an unsandboxed program and would make all programs which are watching relevant directories with permission checks dependencies of the sandboxed program. 

\subsection{Isolation mechanisms}
\label{namespaces}
	In Linux, namespaces\cite{namespaces7} are means of isolation of resources. While these could be used for improving reproducibility somewhat as they can ensure that the sandboxed program
cannot access some dependent resources in the first place. But they do not provide any means with which to trace used files. Furthermore, their usage is essentially an attempt in creating a well-know, reproducible environment in the host system — rather than analysing the existing one.
	
	Control groups\cite{cgroups7} are another way to restrict access to resources for a group of processes. These are more along the lines of hardware resources, open file handles and such. They are an effective sandboxing utility for preventing DOS-like attacks, not for fine grained access control.
	
	Other control mechanisms include user IDs, Group IDs, or Session IDs \cite{credentials7}. These are relevant for reproduction purposes and access control. Not so much for process isolation or tracing.
\subsection{System call interposition}
Another major way to trace events happening is to directly interpose ourselves between the kernel and a user-space application. This has been shown to work with many issues in the past. \cite{garfinkelTraps, 10.5555/1323276.1323278} 

This approach is inherently prone to race conditions since system calls may be executed in parallel. By simply seeing the calls, it is not quite possible to even safely check the parameters of the call with the guarantee that they will not be modified \cite{10.5555/1323276.1323278}.

\subsubsection*{Seccomp}
	
	Seccomp\cite{KernelSeccomp} is a feature relating to sandboxing of programs. One of the things it offers is to install a custom syscall filter. This is used by some programs \cite{ChromiumSeccomp} to voluntarily restrict themselves. 
	
	The filter is written using cBPF a subset of eBPF(extended Barkley packet filter). It is not eBPF, which is commonly used in other parts of the kernel. The filter is inherently stateless. Moreover, there is no way to dereference user-space pointers for data inspection. But while some attempts have been made to extend the kernel to support eBPF it is not yet a part of the mainline kernel. \cite{jia2023programmable}
	
	Even if all of this were added to the kernel, there is no simple way of getting arbitrary-length strings out of eBPF. \cite{EBPF_STRINGS}
	
	So, the filter cannot be used directly to do the logging. However, there are methods for utilising seccomp to invoke other facilities, which could be used.
	
	For one, the filter can say that a given syscall should be audited — SECCOMP\_RET\_LOG. This means that the action will be logged in the audit log. The audit log is a systemd feature\cite{Auditd} and would make systemd a hard dependency\cite{Auditd_prog}. Without it, the seccomp filter will not actually log anything. Furthermore, what data is logged cannot be configured and thus it is unknown if the output would even be consistent across kernel versions.
	
	Another potential result of a filter is that a ptrace (see section \ref{ptrace}) tracer should be notified - SECCOMP\_RET\_TRACE. This comes with other security implications noted down in the manual. Then the behaviour is essentially the same as with a ptrace-based tracer. 
	
	The last potential usage is via SECCOMP\_RET\_USER\_NOTIF which would instead devolve into another process having to execute the operation on behalf of the sandboxed program. This is not really useful for our usecase, unless some form of emulation of syscalls is necessary.

\subsubsection*{Ptrace}	
	\label{ptrace}
	Ptrace\cite{ptrace2} is the Linux syscall for all debugger-related purposes. Just about any debugger-related action is routed through this system call. A program(the tracer) traces another program (the tracee) and can essentially cause arbitrary modifications of the tracee. The tracer can trace any action the tracee performs. A tracee is any individual thread rather than a process, meaning the tracer has to ensure it is attached to all threads.
	
	One limitation of a ptrace-based approach is that it will significantly affect the traced application. Meaning detection of a debugger is possible in many ways. And not only detection. Even the behaviour of some syscalls changes, such as blocking reads, when made by a process that is being debugged. Furthermore, tracing across an suid boundary requires special permissions.
	
\section{Kernel-space approaches}

If I wanted to use some form of tracing from the kernel itself, there is no need to directly modify the kernel code for my needs. The kernel already has some built-in mechanisms for tracing events.

These may have completely different semantics as, for example, path resolution operates on the level of nodes on the filesystem, whereas the user-space API have to actively seek these values to interact with them in this manner.

Any usage of these parameters depends on understanding the specific part of the kernel that is being traced, including all possible ways to bypass it. Incorrect usage could be just as bad, or even worse, than using just plain system call interposition mechanisms.

\subsection{Linux security modules}
Linux security module(LSM) is a framework for a hooking into functionality provided by the kernel at points which may require access rights. They need to be compiled as part of the kernel and cannot be loaded as individual modules at runtime.  \cite{Wright2002LinuxSM}

Compared to a simple system call interposition mechanism, these calls are made in the internals of the kernel when all relevant values have already been transferred into kernel-space and no race conditions should occur.

Instead of writing a full-blown kernel module, it is possible to write eBPF filters for these modules and load them at runtime for modern kernels.\cite{CF_eBPF_LSM,PROG_LSM}
\subsection{Kernel tracing}	
\label{kprobe}
Another avenue is the one used by the tool bpftrace(see section \ref{bpftrace}). 
This involves the usage of some kernel built-in mechanisms for debugging or performance accounting. 

The first approach worth mentioning are kprobes. These are hooks that allow adding a tracing routine to most parts of the kernel. This essentially works as if breakpoints with dedicated handler functions were used with a debugger.\cite{Kprobes}

The other are kernel tracepoints. These are pre-declared places in the kernel which may be traced. Compared to kprobes, these points do not patch the kernel when it is executing, but at compile time. \cite{KernelTracepoints}.


\section{State of the art tracers}
	Some tools which trace program interactions with the kernel and or filesystem  already exist. This section contains description of the most significant ones.
\subsection{bpftrace}
\label{bpftrace}
\emph{Bpftrace}\footnote{https://github.com/bpftrace/bpftrace} is a tool which aggregates a bunch of different tracing mechanisms and adds a unified API for interacting with them.

As the name suggests, the programs for this tool are written in eBPF. But they may be inserted into various parts of the kernel, essentially allowing for debugging of the kernel.
The tracing mechanisms are not limited to just kernel tracing, but to user-space programs and libraries as well.

I have not found a simple way in which to keep a track of processes which should be traced from inside this tool. And tracing the entire kernel is undesirable as it may produce many false-positives.
\subsection{strace}
\emph{Strace}\footnote{https://github.com/strace/strace} is a utility which internally utilises ptrace to track syscalls made by a tracee.

This tool creates a reasonable textual dump of the performed system calls. That is all it really does. Theoretically, a parser could be added on top of strace to then filter out relevant bits of the information it had parsed. But for the purposes of this thesis not all syscalls or all their arguments will be relevant. 

It also shows off that seccomp can be used instead of ptrace to, I presume, ensure the tracer is not notified of all syscalls but only the ones which the BPF program decides should be traced.
\subsection{CARE}
CARE\footnote{https://proot-me.github.io/care/} is a tool built to create an exact reproduction of a program. It creates an archive of all accessed files along with a script which recreates the initial set of environment variables and reruns the command.

This seems to be doing exactly what I am envisioning for this thesis. But the exact approach is very different. To ensure more compatibility, the tool is built over the tool PRoot\footnote{https://proot-me.github.io/}. 

The tool PRoot seems to emulate many necessary system calls to allow unprivileged users to perform privileged operations such as mount or chroot. Essentially translating operations on the filesystem through itself to provide a consistent filesystem view. Glancing at the source code shows that the tool also attempts to keep the API stable in translating ptrace syscalls and trying to allow for nested tracing.

Conceptually, it does what I will be doing with tracking dependencies. I will attempt to avoid any system call emulation if possible.

\subsection{fakechroot}

Fakechroot\cite{fakechroot} is a tool that modifies the c standard library and, in doing so, modifies relevant calls to convince the sandboxed process that the current user is in fact UID 0. The tool does no do tracing in and of itself, but the same mechanism could be used to instead log the actions rather than modify the return values. 

The ways in which the tool can be bypassed have already been mentioned in the chapter \ref{LibShim}.

\iffalse

Tracing at the Kernel boundary consists of tracing of syscalls as those are the only way to communicate between the kernel-space and user-space. \todo{add a quote from Bachors thesis}
This tracing can be done in a couple of ways. 



Strace/Dtrace/seccomp ret with log + audit log/plain old system audit log?/other debugging program/inode watchers/malwayre analysis

ptrace see
https://proot-me.github.io/\#examples	

compared to CARE - what do we do differently?
unmaintained
unknown which syscalls are handled
hwo are special FS handled? 


anything that does not need root access?

See other sandboxes - is a kernel module really needed if we had statically defined the logging rules? Any way to store arbitrary output based on the program data and 

The seccomp output will probably prove to be insufficient.
What is however sufficient is a whole another value
https://www.man7.org/linux/man-pages/man2/seccomp.2.html 3.2.
see SECCOMP\_RET\_TRACE
This is explicitly referenced from 
https://www.man7.org/linux/man-pages/man1/strace.1.html 3.2.
    --seccomp-bpf
              Try to enable use of seccomp-bpf (see seccomp(2)) to have
              ptrace(2)-stops only when system calls that are being
              traced occur in the traced processes.  This option has no
              effect unless -f/--follow-forks is also specified.
              --seccomp-bpf is not compatible with --syscall-limit and
              -b/--detach-on options.  It is also not applicable to
              processes attached using -p/--attach option.  An attempt
              to enable system calls filtering using seccomp-bpf may
              fail for various reasons, e.g. there are too many system
              calls to filter, the seccomp API is not available, or
              strace itself is being traced.  In cases when seccomp-bpf
              filter setup failed, strace proceeds as usual and stops
              traced processes on every system call.  When --seccomp-bpf
              is activated and -p/--attach option is not used,
              --kill-on-exit option is activated as well.

BUGS   
       Programs that use the setuid bit do not have effective user ID
       privileges while being traced.

       A traced process runs slowly (but check out the --seccomp-bpf
       option).

       Unless --kill-on-exit option is used (or --seccomp-bpf option is
       used in a way that implies --kill-on-exit), traced processes
       which are descended from command may be left running after an
       interrupt signal (CTRL-C).

In general it is impossible to use a ptrace-based approach on suid programs as that would result in security issues - it is a debugger after all.

       CAP\_SYS\_PTRACE
              •  Trace arbitrary processes using ptrace(2);
              •  apply get\_robust\_list(2) to arbitrary processes;
              •  transfer data to or from the memory of arbitrary
                 processes using process\_vm\_readv(2) and
                 process\_vm\_writev(2);
              •  inspect processes using kcmp(2).

Possible to do other magic though:
              
           ptrace(PTRACE\_TRACEME, 0, 0, 0);

       turns the calling thread into a tracee.  The thread continues to
       run (doesn't enter ptrace-stop).  A common practice is to follow
       the PTRACE\_TRACEME with

           raise(SIGSTOP);

       and allow the parent (which is our tracer now) to observe our
       signal-delivery-stop.

       If the PTRACE\_O\_TRACEFORK, PTRACE\_O\_TRACEVFORK, or
       PTRACE\_O\_TRACECLONE options are in effect, then children created
       by, respectively, vfork(2) or clone(2) with the CLONE\_VFORK flag,
       fork(2) or clone(2) with the exit signal set to SIGCHLD, and
       other kinds of clone(2), are automatically attached to the same
       tracer which traced their parent.  SIGSTOP is delivered to the
       children, causing them to enter signal-delivery-stop after they
       exit the system call which created them.
                 

THis means that this approach will require some tinkering with privileges from the end-user or, maybe even worse, running the ptrace as root. 
	
though isf we were to run strace as root it is very much so possible to work with this.
 sudo strace -e t=write -qqq -f bash -c 'sudo -u adamep bash -c "sudo cat /etc/shadow"'
will retain the necessary suid properties of sudo. To check that sudo indeed does change the rights the command
sudo strace -e t=write -qqq -f bash -c 'sudo -u adamep bash -c "cat /etc/shadow"'
will not execute properly.

As such root access may be required. On the other hand the root access will only apply to the 


Audit logs also require root access. Moreover they require systemd.
A pure ptrace-based approach? Perhaps with attatching the seccomp manually?
	
though using seccomp for this may result in other issues as simply executing 
sudo strace --seccomp-bpf -e t=write -qqq -f bash -c 'sudo -u adamep bash -c "sudo cat /etc/shadow"'
results in the error:
The "no new privileges" flag is set, which prevents sudo from running as root
	
This happens due to the fact that the seccomp call SECCOMP\_SET\_MODE\_FILTER
If fork(2) or clone(2) is allowed by the filter, any child
              processes will be constrained to the same system call
              filters as the parent.  If execve(2) is allowed, the
              existing filters will be preserved across a call to
              execve(2).

              In order to use the SECCOMP\_SET\_MODE\_FILTER operation,
              either the calling thread must have the CAP\_SYS\_ADMIN
              capability in its user namespace, or the thread must
              already have the no\_new\_privs bit set.  If that bit was
              not already set by an ancestor of this thread, the thread
              must make the following call:
			  
                  prctl(PR\_SET\_NO\_NEW\_PRIVS, 1);
				  
			the reason for this is further explained in the same section
			This requirement ensures that an
              unprivileged process cannot apply a malicious filter and
              then invoke a set-user-ID or other privileged program
              using execve(2), thus potentially compromising that
              program.  (Such a malicious filter might, for example,
              cause an attempt to use setuid(2) to set the caller's user
              IDs to nonzero values to instead return 0 without actually
              making the system call.  Thus, the program might be
              tricked into retaining superuser privileges in
              circumstances where it is possible to influence it to do
              dangerous things because it did not actually drop
              privileges.)
				  
https://www.man7.org/linux/man-pages/man2/seccomp.2.html

and strace simply sets the bit in all cases. see the source code:'
https://github.com/strace/strace/blob/eb6014c510d663938db331fef5ac4ef78fdcf583/src/filter\_seccomp.c\#L85
	/* Get everything ready before PTRACE\_TRACEME.  */
	if (prctl(PR\_SET\_NO\_NEW\_PRIVS, 1, 0, 0, 0) < 0)
		perror\_func\_msg\_and\_die("prctl(PR\_SET\_NO\_NEW\_PRIVS, 1");
	if (prctl(PR\_SET\_SECCOMP, SECCOMP\_MODE\_FILTER, \&prog) < 0)
		perror\_func\_msg\_and\_die("prctl(PR\_SET\_SECCOMP)");
	int pid = getpid();

	if (ptrace(PTRACE\_TRACEME, 0L, 0L, 0L) < 0) {
		/* Exit with a nonzero exit status.  */
		perror\_func\_msg\_and\_die("PTRACE\_TRACEME");
	}

another approach using seccomp would be using SECCOMP\_FILTER\_FLAG\_LOG  or SECCOMP\_FILTER\_FLAG\_NEW\_LISTENER 

https://www.man7.org/linux/man-pages/man2/seccomp\_unotify.2.html
Debugování mi umožňuje provádět prakticky libovolné změny v cílovém procesu a pak respektování suid bitu způsobí chybu.pro strace specificky
sudo strace -e t=write -qqq -f bash -c 'sudo -u adamep bash -c "sudo cat /etc/shadow"'


sudo strace -e t=write -qqq -f bash -c 'sudo -u adamep bash -c "cat /etc/shadow"'

will not execute properly - meaning sudo is working properly.Bez suda
strace -e t=write -qqq -f bash -c 'bash -c "sudo cat /etc/shadow"'
effective uid is not 0, is /usr/bin/sudo on a file system with the 'nosuid' option set or an NFS file system without root privileges?Analogicky pokud se pokusím upravit tvou ukázku, aby spustila cokoliv s suid.    execl("/usr/bin/sudo", "sudo", "/bin/ls /etc/shadow", NULL);


Passwd a další programy skončí stejně.

Ze sekce  execve

       The aforementioned transformations of the effective IDs are not
       performed (i.e., the set-user-ID and set-group-ID bits are
       ignored) if any of the following is true:

       •  the no\_new\_privs attribute is set for the calling thread (see
          prctl(2));

       •  the underlying filesystem is mounted nosuid (the MS\_NOSUID
          flag for mount(2)); or

       •  the calling process is being ptraced.
I kdybychom tohle nějak obešli, například se pokusili program zastavit a pak teprve připojit debugger, bude další problém popsaný sekcí z ptrace.

Deny access if neither of the following is true:

            •  The real, effective, and saved-set user IDs of the target
               match the caller's user ID, and the real, effective, and
               saved-set group IDs of the target match the caller's
               group ID.

            •  The caller has the CAP\_SYS\_PTRACE capability in the user
               namespace of the target.

Deny access if the target process "dumpable" attribute has a
            value other than 1 (SUID\_DUMP\_USER; see the discussion of
            PR\_SET\_DUMPABLE in prctl(2)), and the caller does not have
            the CAP\_SYS\_PTRACE capability in the user namespace of the
            target process.

CAP\_SYS\_PTRACE - umožňuje mimo jiné zapisovat a číst z paměti libovolného procesu...
	
	PID namespace - \proc\{pid}\ means something different. Same with any call which returns the pid.
	
	utilise kcmp for unmatched processes!
\fi