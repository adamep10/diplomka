\chapter{Sources of nondeterminism}
\label{NoNDeter}

As has previously been outlined, I will need to track all possible sources of nondeterminism.
I am interested in what can cause the program to have a different output at the same execution point. By definition, programs are deterministic. Any and all nondeterminism cannot come from the program code but from the hardware it is running on or the environment it is running in.
If I ensured all sources of nondeterminism return the same value, the program would have to produce the same result.

But having a sandbox which would perfectly reproduce the dependencies for one exact program is also meaningless. Recording all the sources of nondeterminism and reproducing them identically is not a good solution.

Not only would this be extremely difficult to pull off in the face of a multithreaded program. If I decided to change anything about the sandboxed program's behaviour — in-line with other access rights already required — the program would not behave as it should since the reproduced API may no longer be semantically correct. After all, it is just a recording with no logic behind it.

I need to recreate the initial conditions so that the program will enact the same behaviour. For potential sources independent of the initial state, the user should at least warned.
	
	In general, I don't consider side channel attacks. No matter what sort of sandbox I construct, side channel attacks are something no sandbox can really help with. Sandboxing is about constricting a specific API. Side channels are creating a new API effectively attacking the policy enforcement. These attacks may exploit various hardware bugs which I cannot easily consider as a part of the provided API.

\section{Hardware}
	The lack of some devices may cause incompatibility, such as a missing GPU. 
	
	On the other hand, a missing audio device might not affect our program at all, as the rest of the operating system will substitute it with another one. But this cannot really be decided as a program could depend on a specific feature of that device. As such, any mismatch in the underlying hardware may be a relevant issue, unless we can decide which exact device we are working with.
	
	Another issue is the internal state of any connected device. I we are communicating with another PC over the network, there is no way to reproduce the state of the other PC. In general this cannot really be solved, and such a sandbox cannot be automatically constructed.
	
	A significant change in the CPU architecture may cause the entire program to be inoperable. This does not have to be any complex feature; even the most basic CPUID\footnote{https://www.felixcloutier.com/x86/cpuid} instruction used to identify a processor and its features can make the actual instructions executed differ.
	
	Realistically, the only way to ensure compatibility in these regards is to make a list of the original hardware specifics and then check them against specifics at sandbox startup.

\section{Kernel}
	The kernel also has some state which may be unobservable from user-space.  At the very least, most kernels should be started relocated to a random address via Kernel Address Space Layout Randomization\cite{KASLR}. Other variables may also apply. Any loaded modules or kprobes(see section \ref{kprobe}), even the exact state of all processes in the OS are saved in the kernel.
	
	But even discounting communication with running processes, previously ran processes could have created shared memory sections which could be accessed by a pre-defined key.\cite{shmget2} 
	
	All sorts of configuration are technically stored on the filesystem but realistically have affected the Kernel when loading. The simplest example is the fstab file\cite{fstab5} which causes a filesystem to be mounted. 
	
	When the sandbox runs, there will be no knowledge of what configurations are used and from where they are used. A prime example are mounts. Mountpoints are not inherently stored on a filesystem — rebooting with a mount will unmount it — and yet they act as directories in many manners.

The semantics of different handled syscalls will be talked about in the tool implementation chapter \ref{syscalls} as they are tied to my solution for handling them.	
	
The semantics behind users and their home directories or even passwords are entirely a user-space mechanism. The kernel merely switches an internal ID associated with a process.\cite{setuid2} 

As user-related values which are relevant for the sandbox should be accessed directly by some process in the sandbox and thus be detectable. This may, however, rely on executing an suid binary and the kernel not interrupting such a tracing mechanism.
	
\section{Filesystems}
	Filesystems are a troublesome terminology. There are three different types of filesystems which need to be named separately.
	
	Firstly, physical filesystems which are persistent filesystems sort on some medium. Secondly, there are virtual filesystems which are used by Linux to provide an API for interaction with parts of the kernel — such as procfs or devfs. Lastly there is the view of these filesystems provided by the Kernel which the users actually interact with.
	When referring to the filesystem, I am referring to the view of the filesystems provided by the kernel.	
	
	All of these systems work like directed acyclic graphs with some nuances. Generally, the structure consists of files and directories. But not all files have to referencible from a filesystem. It is possible to unlink a file and yet until a file descriptor to that file exists, it will not be deleted.
	Physical filesystems may also include a mechanism for unique identification of different entities — e.g. inodes. These can be used, for example, to detect if two files are actually the same file.
	
	Any file that is interacted with can be considered a dependency. But a user does not interact with a single filesystem. In theory, interacting with files from two different physical filesystems may carry its own implications. The trivial implication is that a hard link cannot be created between them. But this may also carry implications for path resolution. The openat syscall\cite{openat22} has a flag RESOLVE\_NO\_XDEV which restricts path resolution to the same physical filesystem.
	
\section{Inherent}
	Some sources of nondeterminism that cannot be properly handled are going to appear due to the nature of the hardware we use nowadays.
	
	Processors contain multiple cache levels. Depending on the state of the caches the processor time necessary to execute a given instruction may vary wildly. As such, any interactions with processor time is bound not to be deterministic. This is used in many timing-based cache attacks.\cite{Lipp2018meltdown,Kocher2018spectre}
	
	Speaking of time, the real current time is also an issue. If a program interacts with the outside world having a valid current time is important, otherwise some authentication algorithms will fail as they give a certain timeframe when a token is valid. Even certificates have a validity interval. So interactions with the current time will inherently lead to nondeterminism which cannot be easily reproduced.
	
	When multiple programs or threads work with the same data or dependencies, the output can end up reordered in the best case. In a worse case the programs have introduced some worse form of a race condition. Depending on the number of cores, the way the scheduler decides to schedule threads, the way caches are set up, the speed of the RAM which contains the data of my program, or even hardware interrupts happening invisible to the sandbox can affect the order or result of operations. There is no reasonable way to create a reproducible environment in the face of these issues unless we forcibly run the program in an environment which can enforce determinism. 
	
	Reading uninitialised memory may be more or less of a issue depending on how it is viewed. The Kernel will zero out any memory which it initially provides to prevent data from being leaked to another process. This can even be an issue on other devices connected to the computer, such as a GPU as demonstrated by \cite{sorensen2024leftoverlocals}.
	 Reading such memory is not inherently an issue. But semantically this means that a piece of memory was allocated and not initialised before being used. Is one of the ways in which programs may begin to act unpredictably. There is no real way for the sandbox to detect this. In theory, this should be reproducible, but any form of nondeterminism in the memory allocator used by the program will lead to unreproducible behaviour.
\section{Reproducing the process sandbox}

At this layer, we are essentially tracking a process — or more specifically a tree of processes stemming from one spawned process.
There are ways in which this tree may interact with other process trees. This can be done via modifying a file used by both groups — such behaviour would be very hard to detect as it is a mostly user-space-based communication. From kernel space we only ever see accesses to the same files, not knowing whether intentional — or unintentional — communication is happening. The methods for tracking accesses to a filesystem outlined in the previous chapter could be used for handling these changes.

A process has multiple variables associated with itself in the kernel. Most of these can be seen as the potential flags of the clone\cite{clone2} call.
\begin{itemize}


\item memory space
\item thread group
\item System V semaphor undo list
\item signal handlers
\item thread local storage
\item the parent PID
\item namespaces
\item debugger attachment
\end{itemize}

Some parts of this list are more easily reproduced than others. Assuming we control the process which spawns the sandboxed program, we can ensure that all but the memory space will be the same. The memory space is impossible to reproduce due to the most basic exploitation protection technique ASLR(address space layout randomisation).

Special care has to be taken when creating a new process as it theoretically could retain some information from the parent. For example, the alarm\footnote{https://www.man7.org/linux/man-pages/man2/alarm.2.html} mechanism is retained across execution of a new program but not clone.

There are also the capabilities of a program that greatly influence the potential operations a program can execute.\footnote{https://www.man7.org/linux/man-pages/man7/credentials.7.html}
Closely tied to this is the no\_new\_privs attribute from prctl\footnote{https://www.man7.org/linux/man-pages/man2/prctl.2.html}.

Another value which cannot be easily managed is the PID of the parent process or even the process itself. This depends on what other programs are running on the system and what state the Kernel is in. This can be mitigated using namespaces(see chapter \ref{namespaces}). Other groups a process is a part of which will be covered later.

\section{Privilege escalatuion}
One of the potential malware evasion techniques is privilege escalation. When a sandbox executes any sort of code which will affect the behaviour of the kernel in unexpected ways, the sandbox cannot guarantee to be complete. The scope of the escalation does not matter at that point. I could affect any layer above the userspace and be undetected due to the layer I have chosen to implement the sandbox at.

\section{Analysis framework detection}
Some tools used to detect that a given executable is running exist. For example the pafish tool \footnote{https://github.com/a0rtega/pafish} for windows checks the following information which could be used to detect the presence of a framework. 

\begin{itemize}


\item registry
\item cpuid instruction
\item timing of various instructions
\item sandbox specific operating system configuration, such as network rules
\item presence of a debugger
\item known usernames
\item drive names, sizes
\item memory size
\item uptime
\item sleep duration consistency
\item presence of user-space hooks — essentially detecting if a DLL injection-like mechanism is happening
\item mouse presence and interaction
\item know sandbox-specific DLLs
\end{itemize}

A similar list of techniques can be found especially for windows \cite{EvasonsList} but categorically they are the same as has been listed so far. 
Other tools for automated detection, such as \footnote{https://github.com/LordNoteworthy/al-khaser}, exist, but they are focused on the Windows platform.

\iffalse
 - current processor time
 - current date/time
 - in what state does the loader leave my executable in
	- is ASLR an issue?
	- can I detect if ASLR is running?
	- all OS flags around my process should be the same for my process
	- how about the initial state of the executable - for example the cstdlib random()
	- will all distributions do the same? Is there any undefined magic?
	- ENV - \$Path and such
	- PID/TID
\fi