\chapter{Malware analysis}
\label{malware}
To make an analysis of malware, we first need to say what malware even is. I will be using the following definition:
\directCiteARG{Malicious code (or malware) is defined as software that
fulfills the deliberately harmful intent of an attacker}{oakland07_explore}{1}

The decision on whether or not any given software is to be considered malware commonly follows the following procedure described in \cite[1]{oakland07_explore} \cite[88:2]{10.1145/3329786}
\begin{enumerate}
\item A signature of the file is calculated. The signature differs depending on which anti-virus checks the given file. Getting a relevant signature is a large part of malware analysis. This signature may be calculated both on the basis of what the software does  on static analysis of the file.
\item If a given signature matches any known software, it is considered to be that type of software — malware or safe.
\item If the signature is not matched, it is then further analysed by a person with the assistance of various tools.
\end{enumerate}

Most of the mechanisms used to get a good signature are not important for this thesis. Nor is the signature matching to other known software a relevant issue. 

The third part of the analysis is the only truly relevant one. I intend to make use of part of the mechanisms used to gather information about the executable rather than decision making. Mechanisms used in manual analysis of a file when automated matching fails can be used for this analysis as well. 

The relevant bits can be tools which gather information about a program in order to restrict further executions of it or a dedicated analyser.\cite{goonasekera2015libvm,fleury2021pdfmalware,acsac2007static} This does not particularly matter.


\section{Determinism}

In general, all programs are by definition deterministic. With the same set of input values, the output shall remain the same. The set of input variables may be very hard, if not for all practical purposes impossible, to reproduce.

All the different inputs can be considered dependencies. Some are more or less reliably reproducible than others. The operating system or modules loaded into it can be considered to be dependencies. So can anything accessible on the filesystem or network. The goal is to reduce these dependencies to a set of values which can be then be restricted to a sandbox or transferred to another system.

Some dependencies may have to be mapped to other resources. For example, the fact that `file descriptor X cannot be written to' is not important in and of itself unless I know what resource the descriptor is referring to. What is important is the fact that the handle is to a file somewhere on the disk and that when it is open, it should only allow these privileges to the given user.

But some things simply cannot be restricted nor reproduced easily. Different hardware setups can be considered a dependency. Some software may only work on specific processor types. Or require having a specific graphics card. Unless I made a list of all the possible quirks and features of hardware components, it is impossible to determine if a given piece of hardware is necessary for the functionality of a program.

Many malware may decide not to employ their payload based on fragments found on the operating system.\cite[88:2]{10.1145/3329786}\cite{pafish}
The values upon which this change of behaviour is based are the exact values that have to be tracked. The dependencies of a program. I have to ensure they are the same in the sandboxed environment as they are in the live environment.

\section{Program analysis methods}

Programs can be analysed using two main approaches — \emph{static} and \emph{dynamic} analysis.\cite[88:2]{10.1145/3329786} Both have some flaws that make them more or less suited for this work.

Two key terms for any sort of analysis are \emph{soundness} and \emph{completeness}. An analysis is sound when it will always mark all occurrences of thing it is meant to detect — it may return false positives.
An analysis is complete when it will never produce false negatives, missing things it is meant to detect.

In an ideal world, a complete and sound analysis would be required. This proves to be impossible as will be explained in the following sections.

A trivial sound analysis would say that the program requires the exact way the filesystem is set up to be the same, or even that the entire computer has to be setup in the same way, including the PIDs of processes.
Such an analysis would surely be sound as there are no files which it could miss. If I were to run the program with the same filesystem state, it would produce — assuming no other dependencies are present — the same output. The analysis is very likely not complete as the odds of having a program depending on all the files on a filesystem are very low.

It is not impossible for that to be the case. A person could launch 
\begin{verbatim}
ls -lR /
\end{verbatim}
and list information about all files on the system. 

\subsection{Static Analysis}

Static analysis works by searching the binary code of the program itself. It attempts to get information about the program without ever having to run the program.

This analysis is particularly vulnerable to many code obfuscation techniques which may cause the analysis to quickly lose precision and relevance. \cite{acsac2007static,10.1145/3329786,Leon2021}

Even if I were able to guarantee that the input program I encounter will not be obfuscated or even that I have access to the source code of it, this will not be sufficient.
There are ways in which the values of variables can be encoded such that resolving these encoded constants to actual useful values end up as an NP-hard problem.\cite{acsac2007static}

Moreover, it will not solve the issues where nondeterminism is introduced into the program at runtime through some other manner.

I could try to enforce that the variables input by the user are constant for this subset of the issue. The naive approach would only suggest that closing the input stream - ensuring EOF is always reached - and a pre-set program environment variables could be sufficient. This would not to consider other avenues in which user-dependent input can be reached in runtime. This is further discussed in \ref{NoNDeter}.

This does not mean that, in a very specific environment, static analysis could not be used for sandboxing. Taking a very specific programming language with constraints such that the invariants which I need to enforce are met could be done. And before loading a program, I would have to prove that the program does not do anything that I would consider malicious. This would, however, still allow for attacking the sandbox itself — as described in section \ref{sandbox attack} the most high level attack vectors attacks either via exploiting the analysis, compiler, or code execution environment. The existence of a general language which meets such criteria would directly contradict Rice's theorem.

Methods for ensuring this can still be done exist. For example the Linux kernel eBPF mechanism \cite{KerneleBPF} does this by not being a true Turing complete language. It restricts the code length, disallows loops and such. Furthermore, it ensures that all interactions with the outside world, the provided API, are done with valid arguments, not that some generic correctness is reached.

\subsection{Dynamic Analysis}

Dynamic analysis consists of actually running the program and observing its behaviour. This will intrinsically only ever cover
a very specific code path.  

A much more sophisticated approach, which attempts to resolve all possible code paths does exist. This is referred to as symbolic execution. 
It involves solving a series of constraints on all code branching points and resolving the branched paths with a new set of constraints.

It is possible to use this in very specific circumstances running in a very specific sandbox to extend dynamic analysis in some ways to attempt to detect code paths executing based on sources of nondeterminism which may be used by malware to avoid detection. \cite[1]{oakland07_explore} 

This is, however, very technically and computationally expensive and is unsuitable for our needs as it needs to be run in a machine which can deterministically execute code even in a multithreaded context and can revert the machine state.


Using just plain dynamic analysis can be done in two ways. Either the program can be run under a debugger for the sake of examining behaviour of specific parts of the code. Or the program can be examined for its interactions with the outside world. \cite[88:2]{10.1145/3329786} Since I assume to work with code which is effectively opaque to me, only the latter approach is viable.

There are security issues bound to using dynamic analysis as it involves running the code in an environment which may be modified or otherwise used for communication by the running code. \cite[88:2]{10.1145/3329786}

Due to the nature of only executing a singular code path, dynamic analysis results will differ wildly depending on the input presented. It is very dependent on any sources of non-determinism. \cite[1]{oakland07_explore}

My dynamic analysis can never be sound and complete for all possible program inputs. It can potentially satisfy these requirements for a very specific set of input values.

\subsection{Manual program analysis}

Manual analysis can either be done by slowly observing the behaviour of a program being run under a debugger. 
Alternatively, some form of sandbox is employed to observe the behaviour of the program being run in a known state.\cite{Bayer2006TTAnalyzeAT}\cite{BareBox}\cite[1-2]{oakland07_explore}

These tools rely on the entire environment being well known. They also require all programs to be transferred to the target. Instead, my tool is mean to run on the target machine and observe the changes made there.
Because of this, the tools cannot be taken and directly reused.

\section{Evading detection}

Malware evasion detection can take many forms depending on which type of analysis it is meant to avoid. 

In the case of attempting to avoid signature-based detection, changing just a couple of bytes can result in the signature being marked different.
And as such is fairly easy to avoid by for example encrypting the program data. \cite[6-7]{oakland07_explore} 
This approach may involve both static and dynamic analysis but is still susceptible to change even if not behavioural change occurs. 

The following detection avoiding mechanisms all have to do with modifying how the program behaves in runtime or otherwise 
In general when a program detects it is running under analysis it may decide to not deploy its payload and instead only act as a normal piece of software.\cite[88:14]{10.1145/3329786}
If I were to detect all possible sources of nondeterminism and successfully recreated them in a new environment, this could be entirely sidestepped as then the program would always act in the same manner.

Another approach to avoiding detection to execute code at a higher protection level.\cite[88:13]{10.1145/3329786} Essentially making some privileged, untraced portions of the sandbox execute code on behalf of the malware itself. Although this may seem like an issue in the design of the sandbox itself and could be avoided if engineered better, it may be an intrinsic part of how the software operates. 
When getting new hardware, a driver for it may have to be loaded into the kernel or be unusable otherwise. However, since this path exists, it can be potentially abused.

If I am talking about sandboxing purely in user-space, any way in which the sandboxed program gets root privileges will have the potential for the root privileges getting abused to avoid detection.

When a sandbox gets constructed it intrinsically has to consider a portion of the code to be immutable and for the codeto  behave in a certain well-known way. This separation will be further discussed in the chapter \ref{sandboxes}. 







\iffalse
The results demonstrate that after the obfuscation pro-
cess, the scanners from Kaspersky and Ikarus were not able
to detect any of the malware instances. Surprisingly for us,
however, the scanners from McAfee and AntiVir were still
able to detect two out of three worms. Closer examination
revealed that the scanner from McAfee detects the two ob-
fuscated samples because of a virus signature that is based
on parts of the data section. When we overwrote the bytes
in the data section that were being used as a signature, the
McAfee scanner could neither detect the original nor the
obfuscated version of the malware anymore. In contrast,
the AntiVir scanner uses a combination of both a data and a
code signature to detect the worms. We were able to track
down the data signature for both Klez.A and MyDoom.A
to a few bytes in the data section. If any of these bytes in
the data section was modified in the obfuscated binary, the
detection by the virus scanner was successfully evaded. In-
deed, it is relatively easy for malicious code to encrypt the
data section using a different key for each instance. Hence,
data signatures are not too difficult to evade. acsac2007static.pdf[6-7]


Privilege escalation—Malware can evade analysis by moving code from one protection ring to a
lower one (by loading a malicious kernel driver, tricking the user into providing the malware with
elevated privileges,18 installing a malicious hypervisor, or infecting a hardware device). Having
more privileges allows malware to perform its malicious tasks with less risk of detection or analysis
(see Section 9). Malware can gain any of the privileges we presented in Section 3.3: 
Kernel—A common technique used by rootkits to infect the kernel is by loading a malicious
kernel driver. This code provides the malware with root privileges, allowing it to hide its presence,
and subvert in-guest analysis components
3329786.pdf[88:13]



Depending on how and where the sandboxing layer gets constructed anything in the trusted code base section getting modified cannot be 
solved by our tool. Kernel/Hypervisor/Firmware are all off limits.


Detecting the analysis framework—One of the most effective ways for malware to evade
analysis is by hiding when it detects that it is under analysis. Since only executed code can be
analyzed, a malware can subvert its normal execution path and hide its malicious activities (e.g.,
instead of installing a keylogger, the malware can simply terminate itself). In this case, the analysis
report produced will not include any trace of the malicious behavior, and the file could be falsely
flagged as benign. Here are a few examples of how malware detect the analysis framework: 3329786.pdf[88:14]

NON DETERMINISM DATA:

Traces of the analysis tool—Analysis tools leave many fingerprints on the system (such as
the names of processes, installed drivers, registry keys, and more[36, 37] 
Debuggers—The presence of a debugger (Section 7.2) is another indication for malware
that it is under analysis. Being a common method for software analysis, the presence of a
debugger is a definite indication that malware analysis is taking place.
Time-based detection—Since at least some overhead is required when analyzing code dy-
namically, the most obvious way to detect analysis is to measure the time it takes to execute
specific segments of code. By comparing the duration of some specific operation to its "nor-
mal" execution time, the malware can expose the existence of an analysis framework.
Traces of users’ activities—When malware infects a personal computer, the malware can
tell whether a user has been using their computer for some time, since this leaves traces
on the system. Analysis environments, however, are frequently wiped clean to allow “a
fresh start” between analyses of different malware. The lack of traces of users’ activities is
sometimes used by malware to detect an analysis environment.20 Checking if the browser’s
history (or the recent documents list) is empty, detecting a “fresh” operating system, testing
if the window of the process is in focus, checking the number of screen monitors connected,
checking the browser history, testing for mouse movement, and so on, have all been seen
in malware.
Other logic bombs—A logic bomb is a piece of code designed to test various conditions about
the environment. If those conditions are met, then it triggers a malicious behavior. Many of the
previous examples are basically logic bombs; other examples of logic bombs are searching for
specific software/hardware, waiting for a specific event (e.g., system shutdown signal), waiting for
a command from the C&C Server, or waiting for a specific date or time.
 3329786.pdf[88:14]
 
 Low-level actions—To evade analysis, malware authors utilize undocumented kernel functions
exported by the operating system to perform their malicious actions. These low-level functions
are implemented by the Windows kernel and can be invoked by user mode processes. Kernel func-
tions are designed to be used only by the operating system itself, and thus these functions are not
documented. Another method of evading analysis is by directly manipulating components of the
OS (e.g., the registry) without calling the proper Windows API functions. This technique requires
advanced skills and a deep understanding of how the OS tracks data, where the data is stored, and
so on. However, a successful attack implementation is very hard to detect and analyze.
Running outside the scope of the analysis tool—There are various ways this can be accom-
plished, including: Code injection -	DLL injection -  DLL search order hijacking - Process hollowing
Infecting the host—If a vulnerability in the system is found by attackers, which allows
malware to escape the guest OS and execute malicious code on the host, then the analysis
can no longer analyze the malicious code. This is the worst-case scenario for the analysis,
since it is very hard to detect and clean host infections.
• Nested virtualization—Malware sometimes create a virtual machine inside an infected
system and execute the malicious code inside the VM, behind the semantic gap (see
Section 7.4). This allows the malware to evade detection and analysis.
  3329786.pdf[88:15]
  
 Avoiding Detection - not interesting to us Code obfuscation, Polymorphism ,  Metamorphism Deep neural networks, Domain generation algorithm
    3329786.pdf[88:16]

 Network evasion, persistence, not really interesting[88:17] 
 
 

Unique CPU Vendor Detection– when the CPUID instruction is executed and the EAX register
contains the value 0, the vendor of the CPU will be written as a 12-charechter ASCII string to
the EBX, ECX and EDX registers. If not configured explicitly to display another value. VMs and
emulators will return distinct unique values such as "VMwareVMware" or
XenVMMXenVMM".
 Timing Attacks – It is possible to use low level APIs and instructions to observe slight changes
in the time it takes to execute a given binary on a physical and on a virtual machine. One
method is calling the RDTSC instruction directly multiple times – while on a physical machine
the returned values from consecutive calls will have very minor diffrences. VMs however, will
typically return values with much higher differnces. However, this test is known to be
somewhat inaccurate.14
 Processor Count – using low-level Windows API enables access to internal OS structures such
as the process environment block (PEB). This structure contains sensetive information about
the machine's hardware including the number of processors it contains. In some sandbox
solutions there is only a single processor, which is very much unlike the majority of modern
PCs – providing malware yet another way in which to determine whether or not it’s in a
sandbox Introduction-to-Evasive-Techniques-v1.0[18]



https://github.com/a0rtega/pafish/tree/master/pafish
 

Dynamic Analysis Properties
To conclude this section about malware analysis, we summarize the required characteristics of any
malware analysis process to minimize the risk of infection and produce accurate results:
• It must be trusted—Data provided by the analysis framework must not be compromised
by the malware [48, 49]. A defensive mechanism must be in place to prevent the malware
from gaining control of the system.
• It must be undetectable by the analyzed file—If malware can tell that it is being ana-
lyzed, then it might terminate itself or execute only non-malicious commands [6].
• It must collect as much relevant data as possible about actions performed by the
malware—Such information can include functions calls, parameters, networking, file sys-
tem modifications, hardware measurements, and so on [1].
• It must meet the malware’s expectations to expose its full behavior - The relevant
operating system(s) must be installed, the appropriate hardware must be connected, and
the vulnerable application must be installed.
• It must limit/emulate network access by the malware—Allowing malware to connect
to internal networks or the Internet must be limited and only take place under supervision
to prevent infection of other devices or exfiltration of sensitive information [80].
• It must generate a coherent and concise report—The information in such report can
be utilized to produce decision regarding the classification of the analyzed file, either by
security expert or machine-learning algorithm.
Despite all of its benefits, analyzing malware dynamically has a few limitations:
• Only executed code is observable. This means that if a required condition is not met pre-
cisely, then some code might not be executed and thus go unanalyzed.
• Dynamic analysis also requires computational overhead, which may slow down execution.
• The analysis must be performed on the specific OS and/or hardware targeted by the mal-
ware. 3329786.pdf[18] 
 


https://dl.acm.org/doi/fullHtml/10.1145/3329786 see for other citation sources especially the part

Although surveys in the domain of malware analysis have been performed in the past, they are either out of date or narrow in scope (e.g., focused on the difference between static and dynamic analysis [2–4]). Another paper surveyed and presented various analysis techniques without going into details [5]; one survey provides a comparison between surveyed tools [6]; another focused on malware propagation methods [7]; and two surveys compared different machine-learning and data-mining studies conducted on malware analysis [8, 9]. In recent years, there has been a large increase in the number of published papers regarding machine-learning-based solutions and the different domains in which they can be applied. Thus, there is no surprise that some studies showed that unknown malware detection can be improved using machine-learning algorithms that provide prediction, generalization, and classification capabilities. The abovementioned surveys may be useful to researchers interested in increasing their knowledge regarding some of the analysis techniques developed in the past, including the core techniques used to dynamically collect relevant data. Such knowledge can be leveraged to enhance malware detection capabilities.

Dynamic analysis [1] refers to the process of analyzing a code or script by executing it and observing its actions. These actions can be observed at various levels, from the lowest level possible (the binary code itself) to the system as a whole (e.g., changes made to the registry or file system). The objective of dynamic analysis is to expose the malicious activity performed by the executable while it is running, without compromising the security of the analysis platform. From the defensive perspective, there is a risk of being infected by the malware while analyzing it dynamically, since it requires that the malware be loaded into the RAM and executed by the hosting CPU.

Dynamic analysis, unlike static analysis, does not rely on analyzing the binary code itself and looks for meaningful patterns or signatures that imply maliciousness of the analyzed file. Such a static approach is vulnerable to many evasion techniques (e.g., packing, obfuscation, etc.). In addition, dynamic analysis does not translate the binary code to assembly level code (AKA disassembling). While a disassembling process may seem straightforward, there are several techniques attackers use to fool the disassembler program and cause it to produce a different assembly code than the one that is actually being executed. By avoiding the disassembling process and by not relying on the analyzed file's binary code itself, dynamic analysis is immune to such malware evasion techniques (for other evasion techniques, see Section 4.2). In addition, while static analysis cannot detect changes made to the code during execution, dynamic analysis is immune to this effect.


 
 
\fi
